%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = 2020-GS-ARTICLE.tex
%----------------------------------------------------------------- LANGUAGES ---
\newcommand{\mylanguages}{italian} % in reverse order
%---------------------------------------------------------- TITLE & SUBTITLE ---
\newcommand{\mytitle}{Retrospettiva Sonora}
\newcommand{\mysubtitle}{appunti per il 2 marzo 2022}
%----------------------------------------------------------------- AUTHOR(s) ---
\newcommand{\authorone}{Giuseppe Silvi}
\newcommand{\institutione}{LEAP}
\newcommand{\emailone}{grammaton @ me.com}
%-------------------------------------------------------------------------------
% \newcommand{\authortwo}{Wikio Orgopedio}
% \newcommand{\institutiontwo}{Conservatorio S. Cecilia di Roma}
% \newcommand{\emailtwo}{wikio @ orgopedio.com} % duplicate these 3 lines if more
%-------------------------------------------------------------- STYLE GS2020 ---
\input{gs2020.tex}
%------------------------------------------------------------ BEGIN DOCUMENT ---
\begin{document}
\maketitle
\thispagestyle{empty}
%-------------------------------------------------------------------- ABSTRACT -
% The abstract is an external txt file inside the includes folder
%-------------------------------------------------------------------------------
%\section*{UNNUMBERED SECTION}

\begin{warn}[repertòrio]
s. m. dal lat. tardo repertorium \emph{lista, catalogo}, der. di reperire
\emph{trovare}\footnote{\url{treccani.it}}.
\end{warn}

Abbiamo una certa familiarità con il termine repertorio in musica al punto che
troviamo la parola \emph{repertorio} nella definizione di \emph{piatto}:
\begin{quote}
\emph{p. forte}, la portata più sostanziosa di un pranzo o di una cena
(e, fig., la parte più importante, più interessante di uno spettacolo,
la parte migliore del repertorio di un artista, e
sim.)\footnote{\url{treccani.it}};
\end{quote}

Si è così abituati a suonare la musica del passato che, nelle discussioni
attuali, si ha necessità invece di specificare cosa sia il contemporaneo,
concettualizzare filososficamente cosa sia contemporaneo, per poi identificare
il significato di una musica contemporanea.

Quando Leonardo Zaccone, nelle infinite discussioni preparatorie, ha
concettualizzato con estrema seplicità che questo evento non sarebbe stato
ascrivibile all'idea di concerto, né di repertorio, tanto meno contemporaneo,
ha individuato nel termine \emph{retrospettiva} la visione più adeguata.
Leonardo è un uomo d'arte, abituato al concetto di retrospettiva. \emph{Io no!}

\begin{warn}[retrospettiva]
s. f. [femm. sost. dell’agg. retrospettivo]. – Mostra retrospettiva: una r.
della scultura novecentesca; in partic., nel linguaggio cinematografico,
rassegna di film che, esaurito il periodo di sfruttamento commerciale, vengono
riproposti in speciali occasioni, di solito in cicli intitolati a un genere,
a un’epoca, a un regista: una r. di R. Clair, del cinema
dell’orrore\footnote{\url{treccani.it}}.
\end{warn}

Io ho dovuto ragionarci un po' sopra. Per me l'idea di retrospettiva musicale non
è molto chiara. Ai limiti del ragionamento c'è la semplicistica osservazione che,
in musica, \emph{retrospettiva} e \emph{repertorio} coincidano.

Credo però ci sia un problema di \emph{medium}, \emph{mezzo}. Il mezzo
in questione non è la musica scritta, il rapporto tra segno e suono di una
partitura estratta dal repertorio. Né una sua esecuzione particolare, una
testimonianza, l'oggetto sonoro costituito dal suono rubato dal tempo per finire
nello spazio di un supporto di scambio, comunicazione, produzione… 

\begin{quote}
  Il m., che ha una sostanziale omogeneità con l'oggetto percepito e l'organo
  senziente, è un elemento essenziale della percezione, perché è proprio
  attraverso il m. che mette in correlazione sia spazialmente che
  qualitativamente senziente e sentito, che ha luogo la ἀλλοίωσις [alterazione]
  e la forma delle cose giunge all'organo del senso (Beare, pp.
  236-239)\footnote{\url{treccani.it}}.
\end{quote}

\emph{Il suono è una sensazione}. Il suono è una sensazione di uno spazio in
vibrazione. Quale spazio? Ogni nostro. Non esiste sensazione fuori dalla nostra
portata; siamo quindi motivati a sostenere che siamo nel suono, siamo il suono,
in maniera piuttosto inafferrabile e imprescindibile. Il suono è in noi e noi
diamo vita alle vibrazioni della materia presente nel nostro spazio, in suono.

La musica che invaderà il nostro spazio, oggi, è una musica fatta di solo suono.
Non ci sono sovrastrutture semantiche, come una notazione indispensabile
all'esecuzione, né sottostrutture necessarie, come un'interprete che dia vita al
suono. Sì! Chiaramente c'è un interprete elettroacustico in grado di dare vita
all'oggetto sonoro (il supporto) affinché si esprima il massimo potenziale
musicale in esso contenuto. Ma ciò che voglio sottolineare è che la composizione
di solo suono \emph{fissata} su un supporto è un'invenzione che conserva delle
peculiarità. Come ho imparato da Risset, un'invenzione basata sul processo di
registrazione audio che ha rivoluzionato il rapporto col suono dell'intera
umanità.

Una musica di solo suono contenuta in un oggetto, \emph{mezzo}, \emph{medium}, è
spazio senza tempo. Solo l'atto di riprodurla, di ri-suonarla, di farla
ri-suonare in un tempo, riporta il tempo nel suono e il suono sensibile nel tempo.

Là dove ho individuato l'inconsistenza del concetto di \emph{retrospettiva
musicale} nasce quindi una peculiarità del repertorio elettroacustico: esiste
una \emph{retrospettiva sonora}. Il suono fissato nello spazio che torna al
tempo-spazio con un proiezione, un ri-suonare, un ridare vita alla sensazione.

Siamo fermi in un punto, guardiamo lo spazio che ci circonda in cerca di una
prospettiva, un ponte che taglia in diagonale il nostro (ipotetico) futuro
cammino. A seguito della decisione "attraverserò quel ponte", quello \emph{sarà}
il nostro cammino. Siamo un punto nel presente in prospettiva futura.

Ora siamo, con il nostro luogo sonoro, con la nostra sensazione immanente,
nel nostro spazio-tempo. Una \emph{prospettiva sonora} è \emph{un'intuizione
uditiva} che sovrascrive lo spazio verso un futuro sensibile, uno scenario
sonoro, un \emph{sonario}, nel quale noi siamo un \emph{adesso} con un vettore
futuro, ci poniamo lungo l'asse temporale \emph{come} un vettore futuro: siamo
qui, ora, ascoltiamo \emph{anche} con i suoni della memoria lo spazio da una
prospettiva.

Oggi, 2 marzo 2022, un punto nello spazio-tempo, ascolteremo una
\emph{retrospettiva sonora} del 2 marzo 1962. Non sarà un viaggio verso la
nostra memoria sonora, nel nostro \emph{sonario} in prospettiva; sarà un viaggio
nel tempo con un vettore opposto, a ritroso nel tempo, nel luogo sonoro passato.
Possiamo immergerci in un ascolto di un altro tempo non sovrastrutturato, fisico,
intimo, micropartecipato, in autonoma sensazione e memoria.

Credo che questa visione del documento sonoro, del medium, del mezzo, riporti e
aggiunga all'insegnamento di Risset un dato importante: la nascita della
registrazione sonora consiste in un momento catastrofico nella storia
dell'umanità; la nascita della composizione sonora \emph{del} suono fissato
nello spazio fuori dal tempo, con la consapevolezza che questo si traduce nella
composizione di uno spazio-tempo nel quale possiamo \emph{poi} immergerci a
piacimento sovrascrivendo il suono già dato, che \emph{non è} senza noi,
consista nella possibilità unica tra le arti della \emph{retrospettiva sonora}
in un viaggio in dietro nel tempo.

% \vfill\null
%
% \raggedright
% \bibliographystyle{unsrt}
% \bibliography{includes/bibliography.bib}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2020 GIUSEPPE SILVI ARTICLE TEMPLATE BASED ON
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Journal Article
% LaTeX Template
% Version 1.4 (15/5/16)
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
% Original author:
% Frits Wenneker (http://www.howtotex.com) with extensive modifications by
% Vel (vel@LaTeXTemplates.com)
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
